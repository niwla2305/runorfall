leaderboard = {}
local storage = minetest.get_mod_storage()

function getKeysSortedByValue(tbl, sortFunction)
    local keys = {}
    for key in pairs(tbl) do table.insert(keys, key) end

    table.sort(keys, function(a, b) return sortFunction(tbl[a], tbl[b]) end)

    return keys
end

function getUsableValues() -- creative
    local sortedKeys = getKeysSortedByValue(storage:to_table().fields,
                                            function(a, b) return a > b end)
    local returnlist = {}
    for _, key in ipairs(sortedKeys) do
        returnlist[#returnlist + 1] = key .. "  -  " ..
                                          storage:to_table().fields[key]
    end

    return returnlist

end

minetest.register_on_joinplayer(function(player)
    playername = player:get_player_name()
    if storage:get_string(playername) == "" then
        storage:set_int(playername, 0)
    end
end)

sfinv.register_page("runorfall:leaderboard", {
    title = "Leaderboard (Beta)",
    get = function(self, player, context)
        local players = {}
        context.myadmin_players = players

        local formspec = {"textlist[0.1,0.1;7.8,8;playerlist;"}

        local is_first = true
        for _, value in pairs(getUsableValues()) do
            local player_name = value
            players[#players + 1] = player_name
            if not is_first then formspec[#formspec + 1] = "," end
            formspec[#formspec + 1] = minetest.formspec_escape(player_name)
            is_first = false
        end
        formspec[#formspec + 1] = "]"

        return sfinv.make_formspec(player, context, table.concat(formspec, ""),
                                   false)
    end
})

function leaderboard.add_points(playername, points)
    storage:set_int(playername, storage:get_int(playername) + points)
end

function leaderboard.add_points_no_privs(playername, points)
    if minetest.check_player_privs(playername, {fly = true}) == true or
        minetest.check_player_privs(playername, {fast = true}) == true then
        return
    else
        leaderboard.add_points(playername, points)
    end
end
