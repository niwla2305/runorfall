local S = minetest.get_translator("gameplay")
runorfall = {}

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)
local spawnpos = {x = 10, y = 102, z = 10}
local startpos = {x = 10, y = 27, z = 10}
local players_ingame = {}
local initial_players_count = 0

local open = io.open
local function read_file(path)
    local file = open(path, "rb") -- r read mode and b binary mode
    if not file then return nil end
    local content = file:read "*a" -- *a or *all reads the whole file
    file:close()
    return content
end

local function write_file(path, content)
    local file = open(path, "wb") -- r read mode and b binary mode
    if not file then return nil end
    file:write(content)
    file:flush()
    file:close()
    return content
end

local function load_map(pos)
    worldedit.deserialize(pos, read_file(modpath .. "/schems/cube_1.we"))
end
local function load_spawn(pos)
    worldedit.deserialize(pos, read_file(modpath .. "/schems/spawn.we"))
end

local function get_ingame_count()
    local counter2 = 0
    for index in pairs(players_ingame) do counter2 = counter2 + 1 end
    return counter2
end

local function startgame(name)
    local player_count2 = get_ingame_count()
    local admin = minetest.get_player_by_name(name)
    if player_count2 == 0 then
        minetest.chat_send_all(minetest.colorize("green", S(
                                                     "Game starts now!!! Be Ready!")))
        load_map({x = 0, y = 0, z = 0})
        for id, player in ipairs(minetest.get_connected_players()) do -- Loop through all players online
            if player:get_meta():get_string("runorfall:afk") == "False" then
                local start_hud = player:hud_add(
                                      {
                        hud_elem_type = "image",
                        position = {x = 0.5, y = 0.5},
                        offset = {x = 0, y = 0},
                        text = "runorfall_game_starts_now.png",
                        alignment = {x = 0, y = 0}, -- center aligned
                        scale = {x = 1.5, y = 1.5} -- covered late
                    })
                minetest.sound_play("runorfall_game_start", {
                    to_player = player:get_player_name(),
                    gain = 2.0
                })
                players_ingame[player:get_player_name()] = true
                minetest.after(3, function()
                    player:set_pos(startpos)
                end)
                minetest.after(4, function()
                    player:hud_remove(start_hud)

                    initial_players_count = get_ingame_count()
                end)
            end
        end
    else
        minetest.chat_send_player(name, S(
                                      "Sorry, the game is already running, you can't start another one at the moment."))
    end
end

minetest.register_on_mapgen_init(function(mgparams)
    minetest.set_mapgen_setting("mg_name", "singlenode", true)
    minetest.settings:set("enable_damage", "false")
end)

local fall_on_timer = function(pos, elapsed)
    local n = minetest.get_node(pos)
    if not (n and n.name) then return true end
    -- Drop trap stone when player is nearby
    local objs = minetest.get_objects_inside_radius(pos, 1.1)
    for i, obj in pairs(objs) do
        if obj:is_player() then
            if n.name == "gameplay:fall" then
                minetest.after(0.6, function(pos)
                    minetest.set_node(pos, {name = "gameplay:fall_2"})
                    minetest.check_for_falling(pos)
                end, pos)
            end
            return true
        end
    end
    return true
end

minetest.register_node("gameplay:side", {
    description = "Side Node",
    tiles = {"runorfall_side.png"},
    light_source = 15
})

minetest.register_node("gameplay:start", {
    description = S("Start Node"),
    tiles = {"runorfall_start.png"},
    on_rightclick = function(pos, node, player, itemstack, pointed_thing)
        local playername = player:get_player_name()
        startgame(playername)
    end
})

minetest.register_node("gameplay:top", {
    description = "Top Node",
    tiles = {"runorfall_side.png"},
    light_source = 15
})

minetest.register_node("gameplay:bottom", {
    description = "Bottom Node",
    tiles = {"^[colorize:#000"},
    light_source = 15
})

minetest.register_node("gameplay:fall", {
    description = "Falling",
    tiles = {"runorfall_fall.png"},
    is_ground_content = false,
    on_construct = function(pos) minetest.get_node_timer(pos):start(0.1) end,
    light_source = 0.1,
    on_timer = fall_on_timer
})

minetest.register_node("gameplay:fall_2", {
    description = "Falling",
    tiles = {"runorfall_fall.png"},
    is_ground_content = false,
    groups = {
        crumbly = 3,
        cracky = 3,
        falling_node = 1,
        not_in_creative_inventory = 1
    },
    drop = ""
})

minetest.register_abm {
    label = "falling remover",
    nodenames = {"group:falling_node"},
    interval = 1,
    chance = 1,
    action = function(pos) minetest.set_node(pos, {name = "air"}) end
}

minetest.register_on_joinplayer(function(player)
    player:get_meta():set_string("runorfall:afk", "False")
    player:set_physics_override({speed = 1.3})
    player:set_pos({x = 0, y = 1200, z = 25})
    minetest.after(2, function() player:set_pos(spawnpos) end)

    player:hud_set_flags({
        hotbar = false,
        healthbar = true,
        crosshair = true,
        wielditem = false,
        breathbar = true,
        minimap = false,
        minimap_radar = false
    })
end)

minetest.register_chatcommand("start", {
    params = "",
    description = "Starts game",
    privs = {shout = true},
    func = function(name, text) startgame(name) end
})

minetest.register_chatcommand("showhud", {
    params = "",
    description = "Shows hudbar",
    privs = {creative = true},
    func = function(name, text)
        local player = minetest.get_player_by_name(name)
        player:hud_set_flags({
            hotbar = true,
            healthbar = true,
            crosshair = true,
            wielditem = true,
            breathbar = true,
            minimap = true,
            minimap_radar = false
        })
    end
})

minetest.register_chatcommand("hidehud", {
    params = "",
    description = "Shows hudbar",
    privs = {creative = true},
    func = function(name, text)
        local player = minetest.get_player_by_name(name)
        player:hud_set_flags({
            hotbar = false,
            healthbar = true,
            crosshair = true,
            wielditem = false,
            breathbar = true,
            minimap = false,
            minimap_radar = false
        })
    end
})

minetest.register_chatcommand("savespawn", {
    params = "",
    description = "Starts game",
    privs = {server = true},
    func = function(name, text)
        local content, count = worldedit.serialize({x = 21, y = 100, z = 0},
                                                   {x = 0, y = 113, z = 21})
        local path = modpath .. "/schems/spawn.we"
        os.remove(path)
        write_file(path, content)
    end
})

-- afk/unafk

minetest.register_chatcommand("afk", {
    params = "",
    description = "Starts game",
    privs = {},
    func = function(name, text)
        player = minetest.get_player_by_name(name)
        player:get_meta():set_string("runorfall:afk", "True")
        minetest.chat_send_all(name .. " is now AFK!")
        player:set_nametag_attributes({
            text = minetest.colorize("#b35a00", "[AFK] ") .. name ..
                minetest.colorize("#b35a00", " [AFK]")
        })
        afk_hud = player:hud_add({
            hud_elem_type = "image",
            position = {x = 0.5, y = 0.5},
            offset = {x = 0, y = 0},
            text = "runorfall_afk.png",
            alignment = {x = 0, y = 0}, -- center aligned
            scale = {x = 1.5, y = 1.5} -- covered late
        })
    end
})

minetest.register_chatcommand("unafk", {
    params = "",
    description = "Starts game",
    privs = {},
    func = function(name, text)
        player = minetest.get_player_by_name(name)
        player:get_meta():set_string("runorfall:afk", "False")
        minetest.chat_send_all(name .. " is no longer AFK!")
        player:hud_remove(afk_hud)
        player:set_nametag_attributes({text = name})
    end
})

minetest.register_chatcommand("kickout", {
    params = "<player>",
    description = "Starts game",
    privs = {server = true},
    func = function(name, text)
        player = minetest.get_player_by_name(text)
        if player == nil then
            minetest.chat_send_player(name, text .. " is not a valid player!")
            return
        end
        executor = minetest.get_player_by_name(name)
        if players_ingame[player:get_player_name()] then
            players_ingame[player:get_player_name()] = nil
        end
        minetest.chat_send_player(name, text .. " was kicked to lobby")
        player:set_pos(spawnpos)
    end
})

--[[
minetest.register_chatcommand("savearena", {
	params = "<text>",
	description = "Saves arena",
	privs = {server = true},
    func = function(name, text)
        local content, count = worldedit.serialize({x=23,y=30,z=0}, {x=0,y=0,z=23})
        write_file(modpath .. "/schems/cube_1.we", content)
	end,
})
--]]

minetest.after(5, function()
    pos = {x = 0, y = 0, z = 0}
    load_map(pos)
end)

minetest.after(1, function()
    pos = {x = 0, y = 100, z = 0}
    load_spawn(pos)
end)

minetest.register_on_leaveplayer(function(player)
    if players_ingame[player:get_player_name()] then
        players_ingame[player:get_player_name()] = nil
    end
end)

minetest.register_globalstep(function(dtime)
    local timer = 0
    if minetest.get_connected_players() == 0 then
        return -- Don't run the following code if no players are online
    end
    timer = timer + dtime
    if timer <= 5 then
        timer = 0
        for id, player in ipairs(minetest.get_connected_players()) do -- Loop through all players online
            if player:get_pos().y < 1 then
                local player_count = get_ingame_count()
                player:set_pos(spawnpos)
                players_ingame[player:get_player_name()] = nil
                if player_count - 1 <= 0 then
                    minetest.chat_send_all(
                        minetest.colorize("red", player:get_player_name() ..
                                              S(" won!")))
                    if initial_players_count > 1 then
                        leaderboard.add_points_no_privs(
                            player:get_player_name(), 50)
                    else
                        leaderboard.add_points_no_privs(
                            player:get_player_name(), 5)
                    end

                else
                    if player_count - 1 == 1 then
                        minetest.chat_send_all(
                            minetest.colorize("yellow",
                                              player:get_player_name() ..
                                                  S(" died! ") .. player_count -
                                                  1 .. S(" player left.")))
                    else
                        minetest.chat_send_all(
                            minetest.colorize("yellow",
                                              player:get_player_name() ..
                                                  S(" died! ") .. player_count -
                                                  1 .. S(" players left.")))
                    end

                    leaderboard.add_points_no_privs(player:get_player_name(), 5)
                end
                minetest.add_particlespawner(
                    {
                        amount = 50,
                        time = 3,
                        minpos = {x = 0, y = 0, z = 0},
                        maxpos = {x = 0, y = 0, z = 0},
                        minvel = {x = 0, y = 0, z = 0},
                        maxvel = {x = 0, y = 0, z = 0},
                        minacc = {x = 0, y = 0, z = 0},
                        maxacc = {x = 0, y = 0, z = 0},
                        minexptime = 1,
                        maxexptime = 1,
                        minsize = 1,
                        maxsize = 1,
                        collisiondetection = false,
                        vertical = false,
                        texture = "runorfall_fall.png"
                    })
            end
        end
    end
end)
