
-- Baked Clay by TenPlus1

local clay = {
    {"natural", "Natural"},
    {"white", "White"},
    {"grey", "Grey"},
    {"black", "Black"},
    {"red", "Red"},
    {"yellow", "Yellow"},
    {"green", "Green"},
    {"cyan", "Cyan"},
    {"blue", "Blue"},
    {"magenta", "Magenta"},
    {"orange", "Orange"},
    {"violet", "Violet"},
    {"brown", "Brown"},
    {"pink", "Pink"},
    {"dark_grey", "Dark Grey"},
    {"dark_green", "Dark Green"},
}
for _, clay in pairs(clay) do

    minetest.register_node("bakedclay:" .. clay[1], {
        description = clay[2] .. " Baked Clay",
        tiles = {"baked_clay_" .. clay[1] ..".png"},
        groups = {cracky = 3, bakedclay = 1},
    })

end

print ("[MOD] Baked Clay loaded")


